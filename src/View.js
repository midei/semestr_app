'use strict'

import {CONTROL_COLUMN, PRIMARY_BUTTON_CLASS, tableData} from "./Const";
import {HtmlTable} from "./HtmlTable";
import {rowFromInputValues, rowFromTd} from "./Utils";
export class View {


    static loadIntoTable({headers, rows}, table) {
        const tableHead = table.querySelector("thead")
        const tableBody = table.querySelector("tbody")
        HtmlTable.clearTable(tableHead, tableBody)

        const headRowElement = document.createElement("tr")
        tableHead.appendChild(headRowElement)

        for (const headerText of headers) {
            HtmlTable.appendChild("th", headerText, headRowElement)
        }
        HtmlTable.appendChild("th", CONTROL_COLUMN, headRowElement)

        for (const row of rows) {
            const bodyRowElement = document.createElement("tr")
            for (const cellText of row) {
                HtmlTable.appendChild("td", cellText, bodyRowElement)
            }
            tableBody.appendChild(bodyRowElement)
            View.#addDelAndEditButtons(bodyRowElement, tableBody)
        }
    }

    static #editButtonListener(bodyRowElement, tableBody) {
        sessionStorage.setItem("editedRow", bodyRowElement.innerHTML)
        View.#editMode(bodyRowElement)
        View.#removeAllButtons(bodyRowElement)
        View.#addOkAndCancelButtons(bodyRowElement, tableBody)
    }

    static #removeAllButtons(bodyRowElement) {
        const buttonElements = bodyRowElement.querySelectorAll('button')
        for (const buttonElement of buttonElements) {
            bodyRowElement.removeChild(buttonElement)
        }
    }

    static #editMode(tableRowElement) {
        const rowTdElements = tableRowElement.querySelectorAll('td')
        const firstElement = rowTdElements[0]
        for (const rowTdElement of rowTdElements) {
            HtmlTable.appendInputForEdit(rowTdElement)
            if (rowTdElement === firstElement)
                rowTdElement.querySelector('input').readOnly = true
        }
    }



    static #addDelAndEditButtons(bodyRowElement, tableBody) {
        const deleteButton = HtmlTable.appendChild("button", "del", bodyRowElement, PRIMARY_BUTTON_CLASS)
        deleteButton.addEventListener("click", () => {
            const rowTdElements = bodyRowElement.querySelectorAll('td')
            const row = rowFromTd(rowTdElements)
            tableData.removeRow(row)
            tableBody.removeChild(bodyRowElement)
        })
        const editButton = HtmlTable.appendChild("button", "edit", bodyRowElement, PRIMARY_BUTTON_CLASS)
        editButton.addEventListener("click", () => {
            View.#editButtonListener(bodyRowElement, tableBody)
        })
    }

    static #addOkAndCancelButtons(bodyRowElement, tableBody) {
        const okButton = HtmlTable.appendChild("button", "ok", bodyRowElement, PRIMARY_BUTTON_CLASS)
        okButton.addEventListener("click", () => {
            const rowInputElements = bodyRowElement.querySelectorAll('input')
            const newRow = rowFromInputValues(rowInputElements)
            tableData.updateRow(newRow)
            View.#returnToReadMode(bodyRowElement, newRow)
            View.#removeAllButtons(bodyRowElement)
            View.#addDelAndEditButtons(bodyRowElement, tableBody)
        })
        const cancelButton = HtmlTable.appendChild("button", "cancel", bodyRowElement, PRIMARY_BUTTON_CLASS)
        cancelButton.addEventListener("click", () => {
            bodyRowElement.innerHTML = sessionStorage.getItem("editedRow")
            View.#removeAllButtons(bodyRowElement)
            View.#addDelAndEditButtons(bodyRowElement, tableBody)
        })
    }

    static #returnToReadMode(tableRowElement, row) {
        const rowTdElements = tableRowElement.querySelectorAll('td')
        for (let i = 0; i < rowTdElements.length; i++) {
            const rowTdElement = rowTdElements[i]
            rowTdElement.innerHTML = ""
            rowTdElement.textContent = row[i]
        }
    }

}



