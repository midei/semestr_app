const femaleFirstNames = ['Mary', 'Linda', 'Barbara', 'Maria', 'Lisa', 'Nancy', 'Betty', 'Sandra', 'Sharon'];
const maleFirstNames = ['James', 'John', 'Robert', 'William', 'David', 'Richard', 'Thomas', 'Paul', 'Mark'];
const cities = ['New York', 'Los Angeles', 'Chicago', 'Las Vegas', 'Austin', 'Tokyo', 'Rio de Janeiro', 'London', 'Paris'];
const cars = ['Honda Civic', 'Toyota Corolla', 'Chevrolet Cruze', 'Honda Accord', 'Nissan Altima', 'Kia Optima', 'Audi A4', 'BMW 750'];
const genders = ['Male', 'Female'];
const namesByGender = {
    Male: maleFirstNames,
    Female: femaleFirstNames
}

export function generateRows(length) {
    const data = []
    for (let i = 0; i < length; i += 1) {
        const record = []
        record.push(i + 1)
        const gender = genders[random( 2)]
        const names = namesByGender[gender]
        record.push(names[random(names.length)])
        record.push(gender)
        record.push(cities[random(cities.length)])
        record.push(cars[random(cars.length)])
        data.push(record);
    }
    return data;
}

function random(length) {
    return Math.floor(Math.random() * (length))
}