import {HEADERS, ROWS, TABLE_HEADERS} from "../Const";

export class TableData {

    static #instance
    #rows = []

    constructor() {
        if (TableData.#instance) {
            return TableData.#instance
        }
        TableData.#instance = this
        Object.freeze(this)
        return this
    }

    setRows(rows) {
        this.#rows = rows
    }

    getTableData() {
        return {[HEADERS]: TABLE_HEADERS, [ROWS]: this.#rows}
    }

    getRowById(id) {
        return this.#rows.find(row => row[0] === id)
    }

    updateRow(newRow) {
        const index = this.#rows.findIndex(row => row[0] === newRow[0])
        if (index !== -1) {
            this.#rows[index] = newRow
        }
    }

    addRow(newRow) {
        this.#rows.push(newRow)
    }

    removeRow(existsRow) {
        const index = this.#rows.findIndex(row => row[0] === existsRow[0])
        if (index !== -1) {
            delete this.#rows[index]
        }
    }

    print() {
        console.log(this.getTableData());
    }
}