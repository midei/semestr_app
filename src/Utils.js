function withIntValuesForInputs(htmlInput) {
    const intValue = parseInt(htmlInput.value)
    if (isNaN(intValue)) return htmlInput.value
    else return intValue
}

function withIntValuesForTd(td) {
    const intValue = parseInt(td.textContent)
    if (isNaN(intValue)) return td.textContent
    else return intValue
}

export function rowFromInputValues(inputs) {
    return Array.from(inputs).map(withIntValuesForInputs)
}

export function rowFromTd(rowTdElements) {
    return Array.from(rowTdElements).map(withIntValuesForTd)
}