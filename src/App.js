import "./style.css"
import "./mui.min.css"
import {Repository} from "./Repository";
import {View} from "./View";
import {CONTROL_COLUMN, tableData} from "./Const";
import {HtmlTable} from "./HtmlTable";
import {rowFromInputValues} from "./Utils";
import {content, hello} from "./HtmlBlocks";

const contentDiv = document.getElementById('content')
const userSession = JSON.parse(sessionStorage.getItem('userSession'))
const banner =  document.getElementById('banner')

//console.log(JSON.stringify(generateRows(8)))

if (userSession) {
    banner.innerText = `Добро пожаловать: ${userSession}`
    contentDiv.innerHTML = content()
    const saveButton = document.getElementById('save-button')
    const inputFile = document.getElementById('input-file')
    const addButton = document.getElementById('add-button')
    const table = document.getElementById('main-table')

    inputFile.addEventListener('change', (e) => {
        const selectedFile = e.target.files[0]
        const reader = new FileReader()
        reader.readAsText(selectedFile)
        reader.onload = function () {
            tableData.setRows(JSON.parse(reader.result))
            View.loadIntoTable(tableData.getTableData(), table)
        }
    })

    saveButton.addEventListener('click', () => {
        Repository.save(tableData.getTableData())
    })



    addButton.addEventListener('click', () => {
        const tableBody = table.querySelector("tbody")
        const theadTh = table.querySelectorAll('thead th')
        const bodyRowElement = document.createElement("tr")
        theadTh.forEach(row => {
            if (row.textContent === CONTROL_COLUMN) return
            HtmlTable.appendInputForAdd(row, bodyRowElement)
        })
        tableBody.appendChild(bodyRowElement)
        bodyRowElement.addEventListener('keypress', function (e) {
            if (e.key === 'Enter') {
                addElementToTable(tableBody, bodyRowElement)
            }
        })
    })


    function addElementToTable(tableBody, bodyRowElement) {
        const addBodyRowElement = document.createElement("tr")
        const tbodyInput = table.querySelectorAll('tbody input')
        const newRow = rowFromInputValues(tbodyInput)
        tableData.addRow(newRow)
        tbodyInput.forEach(row => {
            const cellElement = document.createElement("td")
            cellElement.textContent = row.value
            row.value = ""
            addBodyRowElement.appendChild(cellElement)
        })
        tableBody.insertBefore(addBodyRowElement, bodyRowElement)
    }

    document.addEventListener("load", function () {
        Repository.load().then(data => {
            if (data) {
                tableData.setRows(data.rows)
                View.loadIntoTable(tableData.getTableData(), table)
            }
        })
    }, {capture: true, once: true})
} else {
    banner.innerText = `Представьтесь пожалуйста`
    contentDiv.innerHTML = hello()
    document
        .getElementById('login-form')
        .addEventListener('submit', loginFormHandler, {once: true})

    function loginFormHandler(event) {
        event.preventDefault()
        const user = event.target.querySelector('#user').value
        sessionStorage.setItem("userSession", JSON.stringify(user))
        location.reload(true)
    }
}



