export class Repository {
    static save(data) {
        return fetch("https://semestr-app-default-rtdb.europe-west1.firebasedatabase.app/data.json", {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => response.json())
    }

    static load() {
        return fetch("https://semestr-app-default-rtdb.europe-west1.firebasedatabase.app/data.json", {
            method: "GET",
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => response.json())
    }
}