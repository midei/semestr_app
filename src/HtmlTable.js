export class HtmlTable {
    static clearTable(tableHead, tableBody) {
        tableHead.innerHTML = ""
        tableBody.innerHTML = ""
    }

    static appendChild(tagName, text, parentTag, className) {
        const htmlElement = document.createElement(tagName)
        if (className !== undefined) htmlElement.className = className
        htmlElement.textContent = text
        parentTag.appendChild(htmlElement)
        return htmlElement
    }

    static appendInputForAdd(rowElement, parentElement) {
        const cellElement = document.createElement("td")
        const cellDivElement = document.createElement("div")
        cellDivElement.className = "mui-textfield"
        const cellInput = document.createElement("input")
        cellInput.type = "text"
        cellInput.id = rowElement.textContent
        cellDivElement.appendChild(cellInput)
        cellElement.appendChild(cellDivElement)
        parentElement.appendChild(cellElement)
    }

    static appendInputForEdit(rowElement) {
        const cellDivElement = document.createElement("div")
        cellDivElement.className = "mui-textfield"
        const cellInput = document.createElement("input")
        cellInput.type = "text"
        cellInput.value = rowElement.textContent
        rowElement.textContent = '';
        cellDivElement.appendChild(cellInput)
        rowElement.appendChild(cellDivElement)
    }

}