export function hello() {
    return `
    <form class="mui-form" id="login-form">
      <div class="mui-textfield mui-textfield--float-label">
        <input type="text" id="user" required>
        <label for="user">Имя пользователя</label>
      </div>
      <button
        type="submit"
        class="mui-btn mui-btn--raised mui-btn--primary"
      >
        Войти
      </button>
  `
}

export function content() {
    return `
            <button class="mui-btn mui-btn--primary" id="save-button">Save</button>
            <label class="mui-btn mui-btn--primary">
                <input type="file" id="input-file" accept=".json">
                Load
            </label>
            <button class="mui-btn mui-btn--primary" id="add-button">Add</button>
            <table class="mui-table mui-table--bordered" id="main-table">
                <thead>
                </thead>
                <tbody>
                </tbody>
            </table>
  `
}