import {TableData} from "./model/Models";

export const CONTROL_COLUMN = "Control"
export const PRIMARY_BUTTON_CLASS =  "mui-btn mui-btn--primary"
export const TABLE_HEADERS = ["id", "Name", "Gender", "City", "Car"]
export const HEADERS = "headers"
export const ROWS = "rows"
export const tableData = new TableData()
